
$(function(){
"use strict";

  	// Define Some Elements
  	var allWindow = $(window),
        body = $('body'),
        top = allWindow.scrollTop(),
        navBar = $(".nav-wrapper");

    
    
    var linesHead = $(".skills-section"),
        line = $(".progress-bar-line");
        
    //Progress Bars function
    function progressFunction(e) {

      if ( linesHead.length ) {

        if (!linesHead.hasClass("done")) {

          var linesHeadTop = linesHead.offset().top,
              top = allWindow.scrollTop(),
              winH = allWindow.height() - 160;

          if (top >= linesHeadTop - winH) {

            linesHead.addClass("done");
            $.each( line, function( i, val ) {

            var thisLine = $(this),
              value = thisLine.data("percent"),
              progressCont = $(thisLine).closest('.progress-bar-linear').find(".progress-cont span");

              thisLine.css("width",value + "%");
              progressCont.html(value + "%")

            });
          }
        }
      }
    } //End progressFunction Fuction


    function scrollFunctions() {
      stikyNav();
      ChangeClass();
      parallax();
      progressFunction();
    }

    // add Event listener to window
    allWindow.on('scroll', function() {
      scrollFunctions();
    });


/*------------------------------------------
  Javascript for initialize text Typer
--------------------------------------------*/

    // initialize text Typer Only in Modern browsers
    if (animation) {

      var text = $('#home .typer-title'),
          textOne = "i'm ui/ux designer",
          textTwo = "let's work together",
          textThree = "i can create awesome stuff";

          if (!!$.prototype.typer) {
            text.typer([textOne,textTwo,textThree]);
          }
    }
}


