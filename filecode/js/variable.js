$(function () {


    $('.red').on('click', function () {

        $('#home, #about, #gallery, #tutorial').css({

            'background': 'red',
            'transition': 'all linear .4s'

        });
        $('#home h2').css({

            'color': '#000'
        });

    });
    // red section ends
    $('.blue').on('click', function () {

        $('#home, #about, #gallery, #tutorial').css({

            'background': 'blue',
            'transition': 'all linear .4s'

        });

        $('#home h2').css({

            'color': '#fff'
        });

    });
    // blue section ends

    $('.orange').on('click', function () {

        $('#home, #about, #gallery, #tutorial').css({

            'background': 'orange',
            'transition': 'all linear .4s'

        });

        $('#home h2').css({

            'color': '#fff'
        });

    });
    // orange section ends


    $('.purple').on('click', function () {

        $('#home, #about, #gallery, #tutorial').css({

            'background': 'purple',
            'transition': 'all linear .4s'

        });

        $('#home h2').css({

            'color': '#fff'
        });

    });
    // purple section ends

    $('.normal').on('click', function () {

       $('#home').css({
           'background': 'cornflowerblue'
       });

       $("#about").css({
           'background': 'green'
       });

        $('#gallery').css({
            'background': 'cornflowerblue'
        });

        $('#tutorial').css({
            'background': 'cornflowerblue'
        });

        $('#home h2').css({

            'color': '#fff'
        });

    });
    // normal section ends


    $('.setting-button').on('click', function () {

        $('.color-palatte').toggleClass('r-Zero');


    });

});